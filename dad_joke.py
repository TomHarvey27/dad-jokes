#dad_joke.py
import requests                             # Import the requests library
count = 0                                   # Counter for loop
letters = ["W","I","D","E","E","Y","E","S"] # Letters to create acronym
text_file = open("dadjokes.txt","w")        # Open and create text file

while(count < 8):                           # Loop round once for each letter

    joke_request = requests.get('https://icanhazdadjoke.com', headers={"Accept":"application/json"}) # Acsess the url
    joke_json = joke_request.json();        # Get the json file
    joke = joke_json['joke']                # Get the json joke as a string
    first_Letter = joke[0]                  # Get the first letter of the joke

    if (first_Letter == letters[count]):    # Test to see if the first letter matches the current item in the list
        print(joke)                         # Print to console
        text_file.write(joke + "\n")        # Write each joke on a new line
        count = count + 1                   # Increment counter

text_file.close()                            # Finish writing to file 